package com.hiddentech.grid.objects;

import org.bukkit.Location;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import javax.swing.text.html.parser.Entity;

public abstract class EntityObject extends TickingObject{

    public abstract Entity getEntity();

    public abstract void run(PlayerInteractEntityEvent event);

}

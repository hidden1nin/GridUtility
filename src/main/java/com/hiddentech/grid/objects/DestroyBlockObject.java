package com.hiddentech.grid.objects;

import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public abstract class DestroyBlockObject extends TickingObject {
    public abstract void getBlock();
    public abstract void run(BlockBreakEvent event);
}

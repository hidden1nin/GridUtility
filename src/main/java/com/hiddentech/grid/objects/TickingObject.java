package com.hiddentech.grid.objects;

import org.bukkit.Location;

public abstract class TickingObject {

    public abstract Boolean isLoaded();
    public abstract void load();
    public abstract void unload();
    public abstract void destroy();
    public abstract Location getLocation();
}

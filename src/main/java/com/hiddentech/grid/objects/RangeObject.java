package com.hiddentech.grid.objects;

import org.bukkit.entity.Player;

public abstract class RangeObject extends TickingObject{
    public abstract void run(Player player);
}

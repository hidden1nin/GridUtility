package com.hiddentech.grid.objects;

import org.bukkit.Location;
import org.bukkit.event.player.PlayerInteractEvent;

public abstract class InteractBlockObject extends TickingObject {
    public abstract void getBlock();
    public abstract void run(PlayerInteractEvent event);

}

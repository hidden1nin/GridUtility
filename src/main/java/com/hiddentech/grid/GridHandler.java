package com.hiddentech.grid;

import com.hiddentech.grid.objects.TickingObject;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GridHandler {
    private HashMap<World,ObjectGrid>  worldGridHashMap = new HashMap<>();

    private HashMap<TickingObject, Short>  loaded= new HashMap<>();
    public void loadGrids(){
        for(World world: Bukkit.getWorlds()){
            ObjectGrid grid;
            if(world.getEnvironment()== World.Environment.NETHER){
                world.getWorldBorder().setSize(10000*8);
                grid = new ObjectGrid(new GridBounds(40000, 40000, -40000, -40000), (short) 8, world);

            }else {
                world.getWorldBorder().setSize(10000);
                world.getWorldBorder().setCenter(0, 0);
                grid = new ObjectGrid(new GridBounds(5000, 5000, -5000, -5000), (short) 8, world);
            }
            worldGridHashMap.put(world, grid);

        }
    }
    public void removeNodeFromGrid(TickingObject node, ObjectGrid grid) {
        GridLocation location = grid.getGridLocationFromLocation(node.getLocation());
        if (grid.containsElementInGrid(location, node)) {
            grid.removeElementInGrid(location, node);
            return;
        }
        throw new IllegalArgumentException("Object not in grid!");
    }

    public void placeObjectInGrid(TickingObject node, ObjectGrid grid) {
        grid.insert(node);
    }
    public void placeObjectsInGrid(Map<String, TickingObject> nodes,ObjectGrid grid) {
        for (TickingObject object: nodes.values()) {
            grid.insert(object);
        }
    }
    public Set<TickingObject> getNearbyObjects(Location location){
        return worldGridHashMap.get(location.getWorld()).getNearbyNodes(location);
    }
    public void insertObjects(List<TickingObject> objectList){
        for(TickingObject object:objectList){
            worldGridHashMap.get(object.getLocation().getWorld()).insert(object);
        }
    }
    public void insertObject(TickingObject object){
        worldGridHashMap.get(object.getLocation().getWorld()).insert(object);
    }
    public void removeObject(TickingObject object){
        this.removeNodeFromGrid(object, GridPlugin.getGridHandler().getGrids().get(object.getLocation().getWorld()));
    }
    public HashMap<World,ObjectGrid> getGrids() {
        return worldGridHashMap;
    }

    public HashMap<TickingObject, Short> getLoaded() {
        return loaded;
    }

    public void tick() {
        for(TickingObject object:loaded.keySet()){
           int time =  loaded.get(object);
           if(time<0){
               object.unload();
               loaded.remove(object);
               return;
           }
           time--;
           loaded.put(object, (short) time);
        }
    }
}

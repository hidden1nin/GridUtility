package com.hiddentech.grid;

import com.hiddentech.grid.objects.Grid;
import com.hiddentech.grid.objects.TickingObject;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class GridAPI {
    public HashMap<World,ObjectGrid> getGrids(){
        return GridPlugin.getGridHandler().getGrids();
    }
    public Grid<TickingObject> getGrid(World world){
        return GridPlugin.getGridHandler().getGrids().get(world);
    }
    public Set<TickingObject> getNearbyObjects(Location location){
        return GridPlugin.getGridHandler().getGrids().get(location.getWorld()).getNearbyNodes(location);
    }
    public void insertObjects(List<TickingObject> objectList){
        for(TickingObject object:objectList){
            GridPlugin.getGridHandler().getGrids().get(object.getLocation().getWorld()).insert(object);
        }
    }
    public void insertObject(TickingObject object){
            GridPlugin.getGridHandler().getGrids().get(object.getLocation().getWorld()).insert(object);
    }
    public void removeObject(TickingObject object){
            GridPlugin.getGridHandler().removeNodeFromGrid(object, GridPlugin.getGridHandler().getGrids().get(object.getLocation().getWorld()));
    }
    public void setLoaderPeriod(int millis){
        GridPlugin.getGridPlugin().getLoader().cancel();
        GridPlugin.getGridPlugin().setLoader(new ObjectLoader().runTaskTimer(GridPlugin.getGridPlugin(),20,millis));
    }
}

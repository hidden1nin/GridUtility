package com.hiddentech.grid;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

public final class GridPlugin extends JavaPlugin {
    public static GridHandler getGridHandler() {
        return gridHandler;
    }

    private static GridHandler gridHandler;
    private static GridPlugin gridPlugin;

    private static GridAPI gridAPI;
    public static GridPlugin getGridPlugin() {
        return gridPlugin;
    }
    private BukkitTask loader;
    public static GridAPI getGridAPI() {
        return gridAPI;
    }

    @Override
    public void onEnable() {
        // Plugin startup logic
        this.gridHandler= new GridHandler();
        this.gridPlugin = this;
        this.gridAPI = new GridAPI();
        gridHandler.loadGrids();
        loader = new ObjectLoader().runTaskTimer(this,20,100);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public BukkitTask getLoader() {
        return loader;
    }

    public void setLoader(BukkitTask loader) {
        this.loader = loader;
    }
}

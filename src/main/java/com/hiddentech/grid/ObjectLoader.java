package com.hiddentech.grid;

import com.hiddentech.grid.objects.TickingObject;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ObjectLoader extends BukkitRunnable {
    @Override
    public void run() {

        //this counts down for each object
        GridPlugin.getGridHandler().tick();
        for (Player player : Bukkit.getOnlinePlayers()) {
            for (TickingObject object : GridPlugin.getGridHandler().getGrids().get(player.getWorld()).getNearbyNodes(player.getLocation())) {
                if (!object.isLoaded()) {
                    object.load();
                }
                GridPlugin.getGridHandler().getLoaded().put(object, (short) 3);

            }
        }
    }
}

package com.hiddentech.grid;

import com.hiddentech.grid.objects.Grid;
import com.hiddentech.grid.objects.TickingObject;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.Set;

public class ObjectGrid extends Grid<TickingObject> {
    private short blocksPerBox;
    private World world;
    public ObjectGrid(GridBounds bounds, short blocksPerBox, World world) {
        super(bounds);
        this.world = world;
        this.blocksPerBox = blocksPerBox;
    }

    public ObjectGrid getInstance() {
        return this;
    }

    /*public Set<TickingObject> getNearbyNodes(Location location, String permission) {
        Set<TickingObject> sorted = new HashSet<>();
        for(TickingObject d:this.getSurroundingElements(this.getGridLocationFromLocation(location))){
            if(d.getPermission().equals(permission)){
                sorted.add(d);
            }
        }
        return sorted;
    }*/
    public Set<TickingObject> getNearbyNodes(Location location) {
        return this.getSurroundingElements(this.getGridLocationFromLocation(location));
    }
    public GridLocation getGridLocationFromLocation(Location location) {
        return new GridLocation(this,
                (short) (location.getBlockX() / this.blocksPerBox),
                (short) (location.getBlockZ() / this.blocksPerBox));
    }

    public void insert(TickingObject node) {
        this.insert(this.getGridLocationFromLocation(node.getLocation()), node);
    }

}